<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
// use Response;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Response;
use Twilio\Rest\Client;




class SendSMSController extends Controller
{
    public function index(Request $request)
    {

        $validator = Validator::make($request->all(), [
            'to_number' => 'required'
        ]);
        if($validator->fails()) {
            $error =  json_decode($validator->messages()->toJson(),true);
            return $this->response($error,400);
        }
        $basic  = new \Vonage\Client\Credentials\Basic(ENV('VONAGE_KEY'), ENV('VONAGE_SECRET'));
        $client = new \Vonage\Client($basic);
        $posts = [];
        $message = "";
       
        try {
            $msg = "Hi";
            $response = $client->sms()->send(
                new \Vonage\SMS\Message\SMS($request["to_number"], $msg, 'A text message sent using the Nexmo SMS API ')
            );

            $message = $response->current();

            if ($message->getStatus() == 0) {
                $message =  "The message was sent successfully\n";
            } else {
                $message =  "The message failed with status: " . $message->getStatus() . "\n";
            }
            $posts["message"] = $message;
        }catch (\Exception $e){
            $posts["error_message"] = $e->getMessage();

        }
        return $this->response($posts,200);

    }

    public function send_twilio_message(Request $request){
        $validator = Validator::make($request->all(), [
            'to_number' => 'required'
        ]);
        if($validator->fails()) {
            $error =  json_decode($validator->messages()->toJson(),true);
            return $this->response($error,400);
        }
        $client = new Client(ENV('TWILIO_SID'), ENV('TWILIO_TOKEN'));
        try {
            $client->messages->create(
                $request["to_number"],
                [
                'from' => '+919901352856',
                'body' => 'Hey Jenny! Good luck on the bar exam!'
            ]
            );
            $posts["message"] = "SMS sent successfully";

        }catch (\Exception $e){
            $posts["error_message"] = $e->getMessage();

        }
        return $this->response($posts,200);

    }

    public function response($data,$code){
        return Response::json(
           $data
        , $code);
    }
}
